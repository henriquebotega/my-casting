import { typeOfHeroes } from "../interfaces/IContext";
import IQuestionProps from "../interfaces/IQuestionFilter";

export const isFilterEmpty = (filterValue: IQuestionProps) => {
	return Object.keys(filterValue).length === 0;
};

export const enumToArray = (items: any) => {
	return Object.keys(items);
};

export const getEnumByTitle = (title: any): typeOfHeroes => {
	return Object.keys(typeOfHeroes).find(
		(k: string) => k === title
	) as typeOfHeroes;
};
