import api from "./api";
import IHeroesProps from "../interfaces/IHeroes";

const apikey = `apikey=${process.env.REACT_APP_MARVEL_KEY}`;

export const getCharacterByID = async (id: number) => {
	const { data: responseData } = await api.get(`/characters/${id}?${apikey}`);
	return responseData.data;
};

const returnHeroesByLetter = (data: any, firstLetter: string) => {
	let charactersList: any = [];

	for (var i = 0; i < data.results.length; i++) {
		const obj = data.results[i];

		const currentHero = obj.characters.items.filter((c: IHeroesProps) => {
			return c.name[0].toLowerCase() === firstLetter;
		});

		const jaExiste =
			currentHero.length === 0
				? false
				: charactersList.some((c: IHeroesProps) => {
						return c.name === currentHero[0].name;
				  });

		if (!jaExiste && currentHero.length > 0 && charactersList.length < 10) {
			charactersList.push(currentHero[0]);
		}
	}

	return charactersList;
};

export const loadData = async (
	category: string,
	firstLetter: string
): Promise<any> => {
	const { data: responseData } = await api.get(
		`/${category.toLowerCase()}?${apikey}&limit=100`
	);

	const charactersList = returnHeroesByLetter(
		responseData.data,
		firstLetter.toLowerCase()
	);

	const heroList = charactersList.map(async (currentItem: any) => {
		const uri = currentItem.resourceURI;
		const id = uri.substr(uri.lastIndexOf("/") + 1);

		const { results } = await getCharacterByID(id);
		return results[0];
	});

	return Promise.all(heroList);
};
