import IHeroesProps from "./IHeroes";
import IQuestionProps from "./IQuestionFilter";

export enum typeOfHeroes {
	COMICS = "COMICS",
	SERIES = "SERIES",
	STORIES = "STORIES",
}

interface IContextProps {
	resetHeroes: () => void;
	loadHeroes: () => void;
	setHeroes: (value: IHeroesProps[]) => void;
	sendFilterToParent: (value: IQuestionProps) => void;
	loading: boolean;
	filter: IQuestionProps;
	heroes: IHeroesProps[];
	setFirstLetter: (value: string) => void;
	typeHeroes: typeOfHeroes | undefined;
	setTypeHeroes: (value: typeOfHeroes) => void;
}

export default IContextProps;
