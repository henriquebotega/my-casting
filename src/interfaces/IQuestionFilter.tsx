export interface IQuestionProps {
	questionOne: string;
	questionTwo: string;
}

export default IQuestionProps;
