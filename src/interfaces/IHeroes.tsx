interface IHeroesProps {
	id: number;
	name: string;
	description: string;
	comics: any;
	series: any;
	stories: any;
	thumbnail: { path: string; extension: string };
}

export default IHeroesProps;
