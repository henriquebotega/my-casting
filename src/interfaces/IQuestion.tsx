interface IQuestion {
	title: string;
	options?: any | null;
	sendValueToParent: (value: string) => void;
}

export default IQuestion;
