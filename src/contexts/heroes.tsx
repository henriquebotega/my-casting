import React, { createContext, useContext, useState, ReactNode } from "react";
import IContextProps, { typeOfHeroes } from "../interfaces/IContext";
import IHeroesProps from "../interfaces/IHeroes";
import IQuestionProps from "../interfaces/IQuestionFilter";
import { loadData } from "../services/heroes";

const HeroesContext = createContext({} as IContextProps);

interface IProps {
	children: ReactNode;
}

export const HeroesProvider = ({ children }: IProps) => {
	const [loading, setLoading] = useState(false);
	const [typeHeroes, setTypeHeroes] = useState<typeOfHeroes | undefined>();

	const [firstLetter, setFirstLetter] = useState("");
	const [heroes, setHeroes] = useState<IHeroesProps[]>([]);
	const [filter, setFilter] = useState<IQuestionProps>({} as IQuestionProps);

	const sendFilterToParent = (vlr: IQuestionProps) => {
		setHeroes([]);
		setFilter(vlr);
	};

	const resetHeroes = () => {
		setHeroes([]);
		setTypeHeroes(undefined);
		setFirstLetter("");
	};

	const loadHeroes = async () => {
		setLoading(true);

		if (typeHeroes) {
			try {
				const response = await loadData(typeHeroes, firstLetter);
				setHeroes(response);
			} catch (e) {
				console.log(e);
			}
		}

		setLoading(false);
	};

	return (
		<HeroesContext.Provider
			value={{
				heroes,
				loading,
				filter,
				typeHeroes,
				loadHeroes,
				sendFilterToParent,
				setTypeHeroes,
				setFirstLetter,
				setHeroes,
				resetHeroes,
			}}
		>
			{children}
		</HeroesContext.Provider>
	);
};

export function useHeroes() {
	return useContext(HeroesContext);
}
