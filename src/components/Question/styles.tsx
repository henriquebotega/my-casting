import styled from "styled-components";

export const Container = styled.div`
	padding: 10px;
	margin-right: 10px;

	display: flex;
	justify-content: center;
	align-items: center;
`;

export const Select = styled.select`
	margin-left: 10px;
	border: 1px solid var(--primary);

	&:focus {
		outline: none;
	}
`;

export const Input = styled.input`
	margin-left: 10px;
	border: 0;
	border-bottom: 1px solid var(--primary);

	&:focus {
		outline: none;
	}
`;

export const Option = styled.option``;

export const Title = styled.div`
	font-weight: bold;
`;
