import React, { useState } from "react";
import IQuestion from "../../interfaces/IQuestion";
import { Container, Title, Input, Select, Option } from "./styles";

const Question = ({ title, options, sendValueToParent }: IQuestion) => {
	const [value, setValue] = useState("");

	const handleChangeValue = (e: any) => {
		setValue(e.target.value);
		sendValueToParent(e.target.value);
	};

	return (
		<Container>
			<Title>{title}</Title>

			{options && options.length > 0 && (
				<Select onChange={handleChangeValue} value={value}>
					<Option>Select an option</Option>

					{options.map((op: any) => {
						return (
							<Option key={op} value={op}>
								{op}
							</Option>
						);
					})}
				</Select>
			)}

			{!options && (
				<Input autoFocus value={value} onChange={handleChangeValue} />
			)}
		</Container>
	);
};

export default Question;
