import styled from "styled-components";

type boxType = {
	proportion: boolean;
};

export const Logo = styled.img<boxType>`
	height: ${({ proportion }: boxType) => (proportion ? "150px" : "50px")};
	transition: all 0.4s;
`;
