import React from "react";

import { Logo } from "./styles";

import imageMarvel from "../../assets/logo.jpg";
import { useHistory } from "react-router-dom";

const Header = ({ proportion = true }) => {
	const history = useHistory();

	const goBack = () => {
		history.goBack();
	};

	return <Logo src={imageMarvel} proportion={proportion} onClick={goBack} />;
};

export default Header;
