import styled from "styled-components";

export const Container = styled.div`
	float: left;
	padding: 20px;
	margin: 10px;
	width: 250px;
	background-color: #fff;

	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	border: 1px solid rgba(0, 0, 0, 0.1);
	box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
	transition: all 0.2s;
	border-radius: 5px;

	&:hover {
		border-radius: 0;
		background-color: #eee;
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
	}

	button {
		margin-top: 10px;
		cursor: pointer;
	}
`;

export const Avatar = styled.img`
	width: 200px;
	height: 200px;
`;

export const Button = styled.button``;

export const Title = styled.div`
	font-weight: bold;
	width: 200px;

	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	color: #000;
`;
