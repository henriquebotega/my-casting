import React, { useState } from "react";
import IHeroesProps from "../../interfaces/IHeroes";

import { Container, Title, Avatar, Button } from "./styles";

interface IProps {
	hero: IHeroesProps;
	getQuantity: (value: IHeroesProps) => number;
	openDetails: (value: IHeroesProps) => void;
}

const Card: React.FC<IProps> = ({ hero, getQuantity, openDetails }) => {
	return (
		<Container>
			<Title>{hero.name}</Title>
			<Title>Quantity: {getQuantity(hero)}</Title>

			{hero.thumbnail && (
				<Avatar src={`${hero.thumbnail?.path}.${hero.thumbnail?.extension}`} />
			)}

			<Button onClick={() => openDetails(hero)}>Show details</Button>
		</Container>
	);
};

export default Card;
