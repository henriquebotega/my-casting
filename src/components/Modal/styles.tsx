import styled from "styled-components";

export const Title = styled.div`
	font-weight: bold;
	width: 200px;

	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	color: #000;
`;

export const Description = styled.div``;

export const Button = styled.button``;

export const AvatarModal = styled.img`
	width: 200px;
	height: 200px;
	border-radius: 50%;
`;
