import React from "react";
import IHeroesProps from "../../interfaces/IHeroes";

import { Title, AvatarModal, Description, Button } from "./styles";

const Modal = (setShowModal: any, currentItem?: IHeroesProps) => {
	const renderLatest = (key: string, items?: any) => {
		return (
			<div style={{ marginTop: 10 }}>
				<Title>{key} (Latest 10)</Title>

				{items?.slice(0, 10).map((obj: any, i: any) => {
					return <div key={`${key}_${i.toString()}`}>- {obj.name}</div>;
				})}
			</div>
		);
	};

	return (
		<>
			{currentItem?.thumbnail && (
				<AvatarModal
					src={`${currentItem?.thumbnail?.path}.${currentItem?.thumbnail?.extension}`}
				/>
			)}

			<Title style={{ fontSize: 20 }}>{currentItem?.name}</Title>

			<Description>{currentItem?.description}</Description>

			{renderLatest("Comics", currentItem?.comics?.items)}

			{renderLatest("Series", currentItem?.series?.items)}

			{renderLatest("Stories", currentItem?.stories?.items)}

			<Button onClick={() => setShowModal(false)}>Close</Button>
		</>
	);
};

export default Modal;
