import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import List from "./pages/List";
import Start from "./pages/Start";

const Routes = () => {
	return (
		<BrowserRouter>
			<Route path="/" component={Start} exact />
			<Route path="/list" component={List} />
		</BrowserRouter>
	);
};

export default Routes;
