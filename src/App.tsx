import React from "react";
import { HeroesProvider } from "./contexts/heroes";
import Routes from "./routes";
import Modal from "react-modal";

import "./global.css";

function App() {
	Modal.setAppElement("#root");

	return (
		<HeroesProvider>
			<Routes />
		</HeroesProvider>
	);
}

export default App;
