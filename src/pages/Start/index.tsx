import React, { useEffect, useState } from "react";
import Question from "../../components/Question";
import { useHeroes } from "../../contexts/heroes";
import { enumToArray } from "../../services/util";
import { Container, BtnStart } from "./styles";
import swal from "sweetalert";
import IQuestionProps from "../../interfaces/IQuestionFilter";
import { typeOfHeroes } from "../../interfaces/IContext";
import { useHistory } from "react-router-dom";
import Header from "../../components/Header";

const Start = () => {
	const { sendFilterToParent, resetHeroes, setFirstLetter } = useHeroes();
	const history = useHistory();
	const [started, setStarted] = useState(false);

	const [questionOne, setQuestionOne] = useState("");
	const [questionTwo, setQuestionTwo] = useState("");

	useEffect(() => {
		resetHeroes();
	}, []);

	const start = () => {
		setStarted(true);
		sendFilterToParent({} as IQuestionProps);
	};

	const search = () => {
		if (!questionOne || !questionTwo) {
			swal("You need to fill the fields", "", "error");
			return;
		}

		sendFilterToParent({ questionOne, questionTwo });
		setFirstLetter(questionOne[0]);

		history.push("/list");
	};

	return (
		<Container>
			<Header />

			{!started && (
				<BtnStart id="_btnStart" onClick={start}>
					Let's start
				</BtnStart>
			)}

			{started && (
				<>
					<Question
						title="What is your name?"
						sendValueToParent={setQuestionOne}
					/>

					<Question
						title="What do you like more: comics, series or stories?"
						options={enumToArray(typeOfHeroes)}
						sendValueToParent={setQuestionTwo}
					/>

					<BtnStart id="_btnSearch" onClick={search}>
						Search
					</BtnStart>
				</>
			)}
		</Container>
	);
};

export default Start;
