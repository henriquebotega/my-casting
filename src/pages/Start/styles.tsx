import styled from "styled-components";

export const Container = styled.div`
	background-color: #fff;

	height: 50vh;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;

export const BtnStart = styled.button`
	padding: 10px;
	margin: 10px;
	border: 1px solid var(--primary);
	color: var(--primary);
	transition: all 0.2s;

	&:hover {
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
	}
`;
