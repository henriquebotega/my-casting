import styled from "styled-components";

type boxType = {
	proportion: boolean;
};

export const BoxFloat = styled.div<boxType>`
	position: fixed;
	left: 0;
	top: 0;
	background-color: #fff;

	width: 100%;
	height: ${({ proportion }: boxType) => (proportion ? "35vh" : "15vh")};
	transition: all 0.5s;

	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
`;

export const Content = styled.div`
	margin-top: 225px;

	display: flex;
	flex-direction: column;
	align-items: center;
`;

export const ContentList = styled.div`
	width: 1000px;

	display: flex;
	flex-wrap: wrap;
	align-items: center;
	justify-content: center;

	@media (max-width: 1000px) {
		width: 600px;
	}

	@media (max-width: 700px) {
		width: 400px;
	}
`;

export const BtnReturn = styled.button<boxType>`
	border: 1px solid var(--primary);
	color: var(--primary);

	padding: ${({ proportion }: boxType) => (proportion ? "10px" : "5px")};
	margin: ${({ proportion }: boxType) => (proportion ? "10px" : "5px")};
	transition: all 0.5s;

	&:hover {
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
	}
`;

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;

export const Welcome = styled.div`
	color: #fff;
	margin-bottom: 10px;
`;

export const Title = styled.div`
	font-weight: bold;
	width: 200px;

	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	color: #000;
`;

export const CardAvatar = styled.div`
	float: left;
	padding: 20px;
	margin: 10px;
	width: 250px;
	background-color: #fff;

	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	border: 1px solid rgba(0, 0, 0, 0.1);
	box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
	transition: all 0.2s;
	border-radius: 5px;

	&:hover {
		border-radius: 0;
		background-color: #eee;
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
	}

	button {
		margin-top: 10px;
		cursor: pointer;
	}
`;

export const Description = styled.div``;

export const Button = styled.button``;

export const Avatar = styled.img`
	width: 200px;
	height: 200px;
`;

export const AvatarModal = styled.img`
	width: 200px;
	height: 200px;
	border-radius: 50%;
`;
