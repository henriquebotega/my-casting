import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Header from "../../components/Header";
import { useHeroes } from "../../contexts/heroes";
import IHeroesProps from "../../interfaces/IHeroes";
import { getEnumByTitle, isFilterEmpty } from "../../services/util";
import ReactModal from "react-modal";
import Question from "../../components/Question";

import {
	Container,
	BoxFloat,
	Content,
	BtnReturn,
	ContentList,
	Welcome,
} from "./styles";
import Modal from "../../components/Modal";
import Card from "../../components/Card";

const List = () => {
	const {
		loading,
		heroes,
		loadHeroes,
		setTypeHeroes,
		typeHeroes,
		filter,
		setHeroes,
		resetHeroes,
	} = useHeroes();

	const history = useHistory();

	const [largeHeader, setlargeHeader] = useState(true);
	const [showModal, setShowModal] = useState(false);
	const [orderBy, setOrderBy] = useState("");
	const [currentItem, setCurrentItem] = useState<IHeroesProps | undefined>();

	useEffect(() => {
		if (!isFilterEmpty(filter)) {
			const enumPosition = getEnumByTitle(filter.questionTwo);
			setTypeHeroes(enumPosition);
		}
	}, [filter]);

	useEffect(() => {
		window.addEventListener("scroll", listenScrollEvent);
		return () => window.removeEventListener("scroll", listenScrollEvent);
	}, []);

	useEffect(() => {
		if (!isFilterEmpty(filter) && typeHeroes) {
			loadHeroes();
		}
	}, [typeHeroes]);

	const orderByName = (arr: any) => {
		return arr.sort((a: any, b: any) => {
			if (a.name < b.name) {
				return -1;
			}
			if (a.name > b.name) {
				return 1;
			}
			return 0;
		});
	};

	const orderByQuatity = (arr: any) => {
		return arr
			.map((obj: any) => {
				obj.quantity = getQuantity(obj);
				return obj;
			})
			.sort((a: any, b: any) => b["quantity"] - a["quantity"]);
	};

	useEffect(() => {
		if (orderBy) {
			let reHeroes = [...heroes];

			if (orderBy === "Name") {
				reHeroes = orderByName(reHeroes);
			}

			if (orderBy === "Quantity") {
				reHeroes = orderByQuatity(reHeroes);
			}

			setHeroes(reHeroes);
		}
	}, [orderBy]);

	const listenScrollEvent = (e: any) => {
		if (window.scrollY > 75) {
			setlargeHeader(false);
		} else {
			setlargeHeader(true);
		}
	};

	const goBack = () => {
		resetHeroes();
		history.goBack();
	};

	const openDetails = (e: any) => {
		setCurrentItem(e);
		setShowModal(true);
	};

	const getQuantity = (h: any) => {
		return h.comics.available + h.series.available + h.stories.available;
	};

	const renderWelcome = () => {
		return (
			<Welcome>
				Welcome, {filter.questionOne}. You can choose your favorite character
				filtered by {filter.questionTwo.toLowerCase()} below.
				<Question
					title="Order by"
					options={["Name", "Quantity"]}
					sendValueToParent={setOrderBy}
				/>
			</Welcome>
		);
	};

	return (
		<Container>
			<BoxFloat proportion={largeHeader}>
				<Header proportion={largeHeader} />
				<BtnReturn proportion={largeHeader} onClick={goBack}>
					Reset
				</BtnReturn>
			</BoxFloat>

			{loading && <Content>Loading...</Content>}

			{!loading && (
				<Content>
					{!isFilterEmpty(filter) && renderWelcome()}

					<ContentList>
						{heroes.length === 0 && <div>No results</div>}

						{heroes.length > 0 &&
							!isFilterEmpty(filter) &&
							heroes.map((currentHero: IHeroesProps) => (
								<Card
									key={currentHero.id}
									hero={currentHero}
									getQuantity={getQuantity}
									openDetails={openDetails}
								/>
							))}
					</ContentList>
				</Content>
			)}

			<ReactModal isOpen={showModal} onRequestClose={() => setShowModal(false)}>
				{Modal(setShowModal, currentItem)}
			</ReactModal>
		</Container>
	);
};

export default List;
