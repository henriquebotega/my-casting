import React from "react";
import { shallow } from "enzyme";
import Card from "../components/Card";
import IHeroesProps from "../interfaces/IHeroes";
import { Title } from "../components/Card/styles";

describe("Testing Card Component", () => {
	let currentHero: IHeroesProps;

	beforeEach(() => {
		currentHero = {
			id: 1,
			name: "Anderson",
			description: "test",
			comics: [],
			series: [],
			stories: [],
			thumbnail: { path: "test", extension: "test" },
		};
	});

	afterEach(() => {
		jest.resetAllMocks();
	});

	it("Render component", () => {
		const wrapper = shallow(
			<Card
				hero={currentHero}
				getQuantity={jest.fn()}
				openDetails={jest.fn()}
			/>
		);

		const title = <Title>Anderson</Title>;
		expect(wrapper.contains(title)).toEqual(true);
	});
});
