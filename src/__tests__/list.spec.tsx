import { mount } from "enzyme";
import React from "react";
import App from "../App";
import Card from "../components/Card";
import Modal from "react-modal";
import * as heroesService from "../services/heroes";
import { act } from "react-dom/test-utils";

import ComicsMock from "./comics.mock.json";

describe("Testing List Page", () => {
	beforeEach(() => {
		jest.spyOn(heroesService, "loadData").mockResolvedValue(ComicsMock);
		jest.spyOn(Modal, "setAppElement").mockImplementation(() => {});
	});

	afterEach(() => {
		jest.resetAllMocks();
	});

	it("Should render List", async () => {
		const wrapper = mount(<App />);

		wrapper.find("button").simulate("click");

		const input = wrapper.find("input");
		const select = wrapper.find("select");

		input.simulate("change", {
			target: { value: "Anderson" },
		});

		select.simulate("change", { target: { value: "COMICS" } });
		expect(wrapper.find("select").prop("value")).toEqual("COMICS");
		expect(wrapper.find("input").prop("value")).toEqual("Anderson");

		await act(async () => {
			await wrapper.find("button").simulate("click");
		});

		await wrapper.update();

		expect(wrapper.find(Card)).toHaveLength(3);
		expect(wrapper.find(Card).at(0).prop("hero").id).toEqual(1009156);

		let selectOrder = wrapper.find("select");
		expect(selectOrder).toHaveLength(1);

		selectOrder.simulate("change", { target: { value: "Quantity" } });
		expect(wrapper.find("select").prop("value")).toEqual("Quantity");

		expect(wrapper.find(Card).at(0).prop("hero").id).toEqual(1009165);
		expect(wrapper.text()).toMatch("Show details");
	});
});
