import { mount } from "enzyme";
import React from "react";
import App from "../App";
import Modal from "react-modal";
import Routes from "../routes";

describe("Testing App", () => {
	beforeEach(() => {
		jest.spyOn(Modal, "setAppElement").mockImplementation(() => {});
	});

	afterEach(() => {
		jest.resetAllMocks();
	});

	test("Should check if exists some Routes in your App", () => {
		const wrapper = mount(<App />);
		expect(wrapper.find(Routes).length).toBe(1);
	});
});
