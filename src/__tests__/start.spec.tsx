import React from "react";
import { mount, render } from "enzyme";
import Modal from "react-modal";
import Start from "../pages/Start";
import App from "../App";
import swal from "sweetalert";

jest.mock("sweetalert", () => jest.fn());

describe("Testing Start Page", () => {
	beforeEach(() => {
		jest.spyOn(Modal, "setAppElement").mockImplementation(() => {});
	});

	afterEach(() => {
		jest.resetAllMocks();
	});

	it("Should show a button to start", () => {
		const wrapper = render(<Start />);
		expect(wrapper.text()).toBe("Let's start");
	});

	it("Render component, and should be click in start button", () => {
		const wrapper = mount(<App />);

		wrapper.find("button").simulate("click");
		expect(wrapper.text()).not.toBe("Let's start");
		expect(wrapper.text()).toMatch("What is your name?");
	});

	describe("input requirement", () => {
		let wrapper: any;
		let input: any;
		let select: any;

		beforeEach(() => {
			wrapper = mount(<App />);
			wrapper.find("button").simulate("click");

			input = wrapper.find("input");
			select = wrapper.find("select");

			expect(input).toHaveLength(1);
			expect(select).toHaveLength(1);
		});

		it("Should require to fill the input", () => {
			select.simulate("change", { target: { value: "COMICS" } });

			wrapper.find("button").simulate("click");

			expect(swal).toHaveBeenNthCalledWith(
				1,
				"You need to fill the fields",
				"",
				"error"
			);
		});

		it("Should require to fill the category", () => {
			input.simulate("change", {
				target: { value: "Anderson" },
			});

			expect(wrapper.find("input").prop("value")).toEqual("Anderson");

			wrapper.find("button").simulate("click");

			expect(swal).toHaveBeenNthCalledWith(
				1,
				"You need to fill the fields",
				"",
				"error"
			);
		});

		it("Should fill the input", () => {
			input.simulate("change", {
				target: { value: "Anderson" },
			});

			select.simulate("change", { target: { value: "COMICS" } });

			expect(wrapper.find("select").prop("value")).toEqual("COMICS");
			expect(wrapper.find("input").prop("value")).toEqual("Anderson");

			wrapper.find("button").simulate("click");
			expect(wrapper.text()).toMatch("Loading...");
		});
	});
});
